"""
Miscellaneous utilities.
"""

from tyrell.decider import Example


def example(case):
    """
    Convert a plain-data example to an Example object.

    >>> example({'input': [42, 34], 'output': 27})
    Example(input=[42, 34], output=27)
    """
    return Example(input=case['input'], output=case['output'])
