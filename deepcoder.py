"""
The DeepCoder method, for synthesizing functions from lists to integers.
"""

import pathlib
import re

import demo_deepcoder_enumerator as dc
from jsonrpcserver import method
from tyrell.decider import ExampleConstraintDecider
from tyrell.enumerator import SmtEnumerator
from tyrell.interpreter import GeneralError
from tyrell.spec import parse
from tyrell.synthesizer import Synthesizer
import util

dc.logger.setLevel('DEBUG')

spec_path = pathlib.Path(dc.__file__).parent / 'example' / 'deepcoder.tyrell'
with open(spec_path) as file:
    spec_string = file.read()


def expand(line, constraints):
    r"""
    Use the available constraints to expand the line, if possible.

    Returns a list of lines, which is just a singleton if no expansions
    are available:

    >>> expand('program foo(int, othertype) -> bar;', {})
    ['program foo(int, othertype) -> bar;']

    An expansion specifies a possibly-expanded signature for functions,
    as well as a list of constraints:

    >>> expand('func cons: list -> t, list;', {
    ...     'func cons': ('list b -> t, list a', ['len(b) > len(a)']),
    ... })
    ['func cons: list b -> t, list a {', '  len(b) > len(a);', '}']

    Values can be expanded to add properties, in which case there is no
    signature:

    >>> expand('value type;', {'value type': (None, ['good: bool'])})
    ['value type {', '  good: bool;', '}']
    """
    match = re.match(r'^(?:(value \w+)|(func \w+): .+);\s*$', line)
    if match:
        v, f = match.groups()
        key = v or f
        if key in constraints:
            sig, lines = constraints[key]
            first = ''.join([key, f': {sig}' if sig else '', ' {'])
            return [first] + [f'  {l};' for l in lines] + ['}']
    return [line]


def enhance(spec, constraints):
    """
    Expand an entire spec using the available constraints.

    >>> print(enhance('''enum IntConst {
    ...   "0", "1", "2"
    ... }
    ... value IntValue;
    ...
    ... program BinaryArithFunc(IntValue, IntValue) -> IntValue;
    ...
    ... func const: IntValue -> IntConst;
    ... func plus: IntValue -> IntValue, IntValue;
    ... func minus: IntValue -> IntValue, IntValue;
    ... func mult: IntValue -> IntValue, IntValue;
    ... ''', {
    ...     'func plus': ('IntValue z -> IntValue x, IntValue y',
    ...                   ['is_pos(x) && is_pos(y) ==> is_pos(z)']),
    ...     'value IntValue': (None, ['is_pos: bool']),
    ... }))
    enum IntConst {
      "0", "1", "2"
    }
    value IntValue {
      is_pos: bool;
    }
    <BLANKLINE>
    program BinaryArithFunc(IntValue, IntValue) -> IntValue;
    <BLANKLINE>
    func const: IntValue -> IntConst;
    func plus: IntValue z -> IntValue x, IntValue y {
      is_pos(x) && is_pos(y) ==> is_pos(z);
    }
    func minus: IntValue -> IntValue, IntValue;
    func mult: IntValue -> IntValue, IntValue;
    """
    lines = [line
             for orig in spec.splitlines()
             for line in expand(orig, constraints)]
    return '\n'.join(lines)


constraints = {
    'value int': (None, ['num: int']),
    'value array': (None, ['len: int']),
    'value bool': (None, ['truth: bool']),

    'func head': ('int -> array a', ['len(a) > 0']),
    'func last': ('int -> array a', ['len(a) > 0']),
    'func take': ('array b -> int, array a', ['len(b) <= len(a)']),
    'func drop': ('array b -> int, array a', ['len(b) <= len(a)']),
    'func access': ('int -> int i, array a', ['num(i) >= 0',
                                              'num(i) < len(a)']),
    'func minimum': ('int -> array a', ['len(a) > 0']),
    'func maximum': ('int -> array a', ['len(a) > 0']),
    'func reverse': ('array b -> array a', ['len(b) == len(a)']),
    'func sort': ('array b -> array a', ['len(b) == len(a)']),

    'func map': ('array b -> fn, array a', ['len(b) == len(a)']),
    'func filter': ('array b -> fn, array a', ['len(b) <= len(a)']),
    'func count': ('int n -> fn, array a', ['num(n) <= len(a)']),
    'func zipwith': ('array c -> fn, array a, array b', ['len(c) <= len(a)',
                                                         'len(c) <= len(b)']),
    'func scanl1': ('array b -> fn, array a', ['len(b) == len(a)']),

    'func pos': ('int y -> int x', ['num(y) == num(x)']),
    'func neg': ('int y -> int x', ['num(y) == -num(x)']),
    'func plus': ('int z -> int x, int y', ['num(z) == num(x) + num(y)']),
    'func minus': ('int z -> int x, int y', ['num(z) == num(x) - num(y)']),
    'func mul': ('int z -> int x, int y', ['num(z) == num(x) * num(y)']),
    'func div': ('int z -> int x, int y', ['num(y) != 0',
                                           'num(z) == num(x) / num(y)']),
    'func pow': ('int z -> int x, int y', ['num(x) > 0',
                                           'num(y) >= 0',
                                           'num(z) >= 1',
                                           'num(y) > 0 ==> num(z) >= num(x)']),
    'func gt_zero': ('bool p -> int x', ['num(x) > 0 ==> truth(p)',
                                         'num(x) <= 0 ==> !truth(p)']),
    'func lt_zero': ('bool p -> int x', ['num(x) < 0 ==> truth(p)',
                                         'num(x) >= 0 ==> !truth(p)']),
    'func is_even': ('bool p -> int x', ['num(x) % 2 == 0 ==> truth(p)',
                                         'num(x) % 2 != 0 ==> !truth(p)']),
    'func is_odd': ('bool p -> int x', ['num(x) % 2 != 0 ==> truth(p)',
                                        'num(x) % 2 == 0 ==> !truth(p)']),
}
spec = parse(enhance(spec_string, constraints))


class ConstrainedDeepCoderInterpreter(dc.DeepCoderInterpreter):
    """
    An interpreter for DeepCoder with SMT-accessible runtime assertions.
    """

    def apply_num(self, val):
        return val

    def apply_len(self, val):
        return len(val)

    def apply_truth(self, val):
        return val

    def eval_head(self, node, args):
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: len(x) > 0,
        )
        return super().eval_head(node, args)

    def eval_last(self, node, args):
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: len(x) > 0,
        )
        return super().eval_last(node, args)

    def eval_access(self, node, args):
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: x >= 0,
        )
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: x < len(args[1]),
            capture_indices=[1],
        )
        return super().eval_access(node, args)

    def eval_minimum(self, node, args):
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: len(x) > 0,
        )
        return super().eval_minimum(node, args)

    def eval_maximum(self, node, args):
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: len(x) > 0,
        )
        return super().eval_maximum(node, args)

    def eval_div(self, node, args):
        # to prevent an IndexError from breaking the interpreter when
        # trying to use assertArg
        if len(args) < 2:
            raise GeneralError()
        self.assertArg(
            node, args,
            index=1,
            cond=lambda x: x != 0,
        )
        return super().eval_div(node, args)

    def eval_pow(self, node, args):
        if len(args) < 2:
            raise GeneralError()
        self.assertArg(
            node, args,
            index=0,
            cond=lambda x: x > 0,
        )
        self.assertArg(
            node, args,
            index=1,
            cond=lambda x: x >= 0,
        )
        return super().eval_pow(node, args)


dc_interpreter = ConstrainedDeepCoderInterpreter()


@method
def deepcoder(test, code=None, type=None, prose=None):
    dc.logger.info(f'Received test: {test}')
    dc.logger.info('Building enumerator')
    # this takes a long time to do, but even though constructing the
    # enumerator doesn't take the tests as input, it has to be
    # constructed on each request because otherwise it simply exhausts
    # all programs and then can't run anymore on future requests
    smt_enumerator = SmtEnumerator(spec, depth=5, loc=5)
    dc.logger.info('Done building enumerator')
    synthesizer = Synthesizer(
        enumerator=smt_enumerator,
        decider=ExampleConstraintDecider(
            spec=spec,
            interpreter=dc_interpreter,
            examples=list(map(util.example, test)),
        )
    )
    return {
        'code': str(synthesizer.synthesize()),
    }
