#!/usr/bin/env python3

"""
Print a sample request, send it to the Muse, and print the response.
"""

import sys

from jsonrpcclient.clients.http_client import HTTPClient
from jsonrpcclient.requests import Request

tests = {
    'deepcoder': [{'input': [[6, 2, 4, 7, 9], [5, 3, 6, 1, 0]], 'output': 27}],
    'toy': [
        {'input': [4, 3], 'output': 3},
        {'input': [6, 3], 'output': 9},
        {'input': [1, 2], 'output': -2},
        {'input': [1, 1], 'output': 0},
    ],
}

if __name__ == '__main__':
    if len(sys.argv) < 2:
        exit('Please specify a method.')
    method = sys.argv[1]
    client = HTTPClient('http://localhost:5000')
    request = Request(method, test=tests[method])
    print(f'--> {request}')
    print(f'<-- {client.send(request).data}')
