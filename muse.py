#!/usr/bin/env python3

"""
JSON-RPC entrypoint.
"""

import contextlib
from http.server import BaseHTTPRequestHandler
import io
from kitchensink.utility.forkinghttpserver import ForkingHTTPServer
import logging
import os
import subprocess
from socketserver import ForkingMixIn
import sys



import deepcoder
from jsonrpcserver import dispatch
import toy

class TrinityHttpServer(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'  # to allow persistent connections

    def do_POST(self):
        # Process request
        request = self.rfile.read(int(self.headers["Content-Length"])).decode()
        logging.debug('Received request')
        response = dispatch(request)
        logging.debug('Computed response')
        content = str(response).encode('utf-8')
        # Return response
        self.send_response(response.http_status)
        self.send_header("Content-Length", len(content))
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        self.wfile.write(content)
        logging.debug('Sent response')


if __name__ == "__main__":
    # to see the port number
    logging.basicConfig(level=logging.INFO)
    port = int(os.environ['PORT'] or '5000') if 'PORT' in os.environ else 5000
    logging.info(" * Listening on port %s", port)
    with ForkingHTTPServer(("0.0.0.0", port), TrinityHttpServer) as s:
        s.serve_forever()
