"""
The Toy method, for synthesizing trivial arithmetic expressions.
"""

import pathlib

import demo_smt_enumerator as demo
from jsonrpcserver import method
from tyrell.decider import ExampleConstraintDecider
from tyrell.dsl.node import ApplyNode, AtomNode, ParamNode
from tyrell.enumerator import SmtEnumerator
from tyrell.spec import parse_file
from tyrell.synthesizer import Synthesizer
import util
import sexpdata

demo.logger.setLevel('DEBUG')

spec_path = pathlib.Path(demo.__file__).parent / 'example' / 'toy.tyrell'
spec = parse_file(spec_path)

@method
def toy(*, test, code=None, type=None, prose=None, depth=3, loc=2):
    demo.logger.info(f'Received test: {test}')
    demo.logger.info('Building enumerator')
    smt_enumerator = SmtEnumerator(spec, depth=depth, loc=loc)
    demo.logger.info('Done building enumerator')
    synthesizer = Synthesizer(
        enumerator=smt_enumerator,
        decider=ExampleConstraintDecider(
            spec=spec,
            interpreter=demo.ToyInterpreter(),
            examples=list(map(util.example, test)),
        ),
    )
    return {
        'code': sexpdata.dumps(synthesizer.synthesize().to_sexp())
    }
