FROM ubuntu:20.04

# install prerequisites for Trinity; r-base depends on tzdata
# https://stackoverflow.com/a/44333806
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  git python3-dev python3-venv r-base

# setup Trinity
ARG TRINITY_COMMIT
RUN git clone https://github.com/fredfeng/Trinity.git
WORKDIR /Trinity
RUN git checkout $TRINITY_COMMIT
RUN mkdir venv
ENV VIRTUAL_ENV "$PWD/venv"
RUN python3 -m venv "$VIRTUAL_ENV"
# should be essentially equivalent to `source venv/bin/activate`
# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
ENV PATH "$VIRTUAL_ENV/bin:$PATH"

# another prerequisite not mentioned in the Trinity README
RUN pip install wheel

RUN pip install -e ".[dev]"

# done with Trinity
WORKDIR /

# install other Python requirements needed for this Muse
COPY requirements.txt .
RUN pip install -r requirements.txt
