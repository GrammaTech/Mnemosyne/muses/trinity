# Trinity

This Muse provides an interface to the [Trinity][] synthesizer.

## Setup

First, be sure to clone this repository using `--recurse-submodules`, since the
`Trinity` submodule is required for usage. After cloning, `cd` into the
`Trinity` subdirectory and follow the setup instructions from the Trinity README
to create the virtual environment and install dependencies. (Note that you may
need to insert [an extra step][pip install wheel].)

Staying in the **same virtual environment**, `cd` back up to this directory and
install the extra requirements needed on top of those of Trinity itself:
```
$ pip install -r requirements.txt
```
(See the [Dockerfile][] in this repository for more details, but note that the
Dockerfile is meant to include only the dependencies of this repository rather
than the entire build, and thus makes use of the Trinity Git submodule only
indirectly. See [below](#docker-images) for more notes about the Dockerfile.)

You no longer need to be in the Trinity virtual environment for the rest of
these instructions, unless noted otherwise. Finally, to install this Muse
globally:
```
$ sudo ln -s {"$PWD",/usr/local}/bin/trinity-muse
```

## Usage

Simply run the command with no arguments from any directory:
```
$ trinity-muse
[debug] Building Tyrell spec from parse tree...
[debug] Processing type definitions...
[debug] Processing input/output definitions...
[debug] Processing function definitions...
[debug] Processing global predicates...
[debug] Building Tyrell spec from parse tree...
[debug] Processing type definitions...
[debug] Processing input/output definitions...
[debug] Processing function definitions...
[debug] Processing global predicates...
INFO:root: * Listening on port 5000
```
This will start a [JSON-RPC][] server at http://localhost:5000 which accepts
`"params"` containing at least the key `"test"` (and optionally also any subset
of the keys `"code"`, `"type"`, and `"prose"`, all of which will be ignored) and
returns a `"result"` containing a `"code"` key whose value is code to synthesize
the given `"test"`.

The value for `"test"` must be an array of test cases, each of which is an
object containing an `"input"` (which is always an array) and an `"output"`,
whose formats are determined by the `"method"` being used; see below.

### Methods

These are the allowed values for `"method"`, along with their
`"input"`/`"output"` formats:

- **`"deepcoder"`:** The `"input"` must have two elements, each of which is an
  array of integers. The `"output"` must be an integer.
- **`"toy"`:** The `"input"` must have two elements, each of which is an
  integer. The `"output"` must be an integer.

### Example

While having the Muse running in one terminal, open another terminal in this
directory and run
```
$ ./client.py deepcoder
--> {"jsonrpc": "2.0", "method": "deepcoder", "params": {"test": [{"input": [[6, 2, 4, 7, 9], [5, 3, 6, 1, 0]], "output": 27}]}, "id": 1}
<-- {"jsonrpc": "2.0", "result": {"code": "maximum(zipwith(get_fn(plus), map(get_mfn(mul, 3), @param0), @param1))"}, "id": 1}
```
to print a sample request, send it to the Muse, and print the JSON-RPC response.
The Muse (in the original terminal) should print the `"test"` it received
followed by logging info while it searches for a program.

The above example (using the `"deepcoder"` method) is somewhat long-running due
to the complexity of its DSL; the `"toy"` method (for doing simple arithmetic)
should be bit quicker:
```
$ ./client.py toy
--> {"jsonrpc": "2.0", "method": "toy", "params": {"test": [{"input": [4, 3], "output": 3}, {"input": [6, 3], "output": 9}, {"input": [1, 2], "output": -2}, {"input": [1, 1], "output": 0}]}, "id": 1}
<-- {"jsonrpc": "2.0", "result": {"type": "PTerm", "code": ["POp", ["EmptyFC"], ["UN", "*"], ["PRef", ["EmptyFC"], ["UN", "y"]], ["POp", ["EmptyFC"], ["UN", "-"], ["PRef", ["EmptyFC"], ["UN", "x"]], ["PRef", ["EmptyFC"], ["UN", "y"]]]]}, "id": 1}
```
As shown here, a feature of the `"toy"` method is that it returns its output as
an [Argot][] Core AST (along with the `"type"` of that AST node, which in this
case is always `"PTerm"`), which can then be easily translated into a base
language.

## Development

To work on this Muse, you'll need to reenter the virtual environment you created
in the submodule directory:
```
$ source Trinity/venv/bin/activate
```

### Running the tests

This should run all the doctests, and any tests in `test_*.py`:
```
$ pytest
```

### Adding methods

The entrypoint to this Muse is in [`muse.py`][], which just imports the Python
file for each of the JSON-RPC methods and then starts the server. Thus, to add a
new method `foo`, simply create a Python file `foo.py` and add
```python
import foo
```
to `muse.py`. The new file `foo.py` should include
```python
from jsonrpcserver import method
```
in its imports, and at the bottom should define a function that looks like this:
```python
@method
def foo(test, code=None, type=None, prose=None):
    # ...
    return {
        'code': # ...
    }
```

### Docker images

The [Dockerfile][] builds an environment with all of the dependencies for both
Trinity itself and this wrapper specifically:
```
$ TAG=registry.gitlab.com/grammatech/mnemosyne/muses/trinity
$ docker build --build-arg TRINITY_COMMIT="$(./trinity_commit.sh)" -t "$TAG" .
```
Note that on each commit (on any branch), CI builds an image, tags it as
`registry.gitlab.com/grammatech/mnemosyne/muses/trinity:latest` (as shown in the
above commands), and pushes it to the container registry for this repo. Not
shown here is the fact that the CI-built image uses the previous pushed image
(of the same tag) as a cache, so it shouldn't change at all unless the
dependencies change. However, because there is only one tag, the cache will not
work as intended if there are multiple branches being developed concurrently
with different Dockerfiles. To avoid that issue, it may be a good idea to modify
the CI setup to instead have a separate Docker image tag for each branch.

[`muse.py`]: muse.py
[argot]: https://gitlab.com/GrammaTech/Mnemosyne/argot
[dockerfile]: Dockerfile
[json-rpc]: https://en.wikipedia.org/wiki/JSON-RPC
[pip install wheel]: https://github.com/fredfeng/Trinity/pull/7/files#diff-04c6e90faac2675aa89e2176d2eec7d8R18
[trinity]: https://github.com/fredfeng/Trinity
