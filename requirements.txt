jsonrpcclient==3.3.6
jsonrpcserver==4.1.3
pytest==6.0.0
git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink
